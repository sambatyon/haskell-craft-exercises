-- Exercise 3.18
onThreeLines :: String -> String -> String -> String
onThreeLines a b c = a ++ "\n" ++ b ++ "\n" ++ c

-- Exercise 3.19
number2roman :: Int -> String
number2roman n
  | n == 0 = ""
  | n - 1000 > 0 = "M" ++ number2roman (n - 1000)
  | n - 900 > 0 = "CM" ++ number2roman (n - 900)
  | n - 500 > 0 = "D" ++ number2roman (n - 500)
  | n - 400 > 0 = "CD" ++ number2roman (n - 400)
  | n - 100 > 0 = "C" ++ number2roman (n - 100)
  | n - 90 > 0 = "XC" ++ number2roman (n - 90)
  | n - 50 > 0 = "L" ++ number2roman (n - 50)
  | n - 40 > 0 = "XL" ++ number2roman (n - 40)
  | n - 10 > 0 = "X" ++ number2roman (n - 10)
  | n - 9 > 0 = "XL" ++ number2roman (n - 9)
  | n - 5 > 0 = "V" ++ number2roman (n - 5)
  | n - 4 > 0 = "IV" ++ number2roman (n - 4)
  | otherwise = "I" ++ number2roman (n - 1)

romanDigit :: Char -> String
romanDigit ch
  | n <= 0 || n > 9 = "Error"
  | otherwise = number2roman n
  where n = fromEnum ch - fromEnum '0'
